import AdminJS from 'adminjs'
import AdminJSExpress from '@adminjs/express'
import express from 'express'
import { Database, Resource } from '@adminjs/mongoose' // or any other adapter
import mongoose from 'mongoose'
import AdminBro from 'admin-bro'

AdminJS.registerAdapter({ Database, Resource })

const PORT = 3001
const User = mongoose.model('User', { name: String, email: String, surname: String })
const Admin = mongoose.model('Admin', { name: String, email: String, password: String})

const start = async () => {
  const app = express()
  const mongooseDb = await mongoose.connect('mongodb://localhost:27017', { useNewUrlParser: true })

  const adminBro = new AdminJS({
    databases: [mongooseDb],
    rootPath:'/admin',
    branding: {
      companyName: 'NowYouKnow',
    },
  })
  
  const adminRouter = AdminJSExpress.buildRouter(adminBro)
  app.use(adminBro.options.rootPath, adminRouter)

  app.listen(PORT, () => {
    console.log(`AdminJS started on http://localhost:${PORT}${adminBro.options.rootPath}`)
  })
}

start()